package PL;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author DEI-ESINF
 */
public class TREE_WORDS extends BST<TextWord> {

    public void createTree() throws FileNotFoundException {
        try (Scanner readfile = new Scanner(new File("src/PL/xxx.xxx"))) {
            while (readfile.hasNextLine()) {
                String[] pal = readfile.nextLine().split("(\\,)|(\\s)|(\\.)");
                for (String word : pal) {
                    if (word.length() > 0) {
                        insert(new TextWord(word, 1));
                    }
                }
            }
        }
    }

    /**
     * Inserts a new word in the tree, or increments the number of its
     * occurrences.
     *
     * @param element
     */
    @Override
    public void insert(TextWord element) {
        root = insert(element, root);
    }

    private Node<TextWord> insert(TextWord element, Node<TextWord> node) {
        if (node == null) {
            return new Node(element, null, null);
        }

        if (element.compareTo(node.getElement()) < 0) {
            if (node.getLeft() == null) {
                node.setLeft(new Node<>(element, null, null));
                return node.getLeft();
            }
            return insert(element, node.getLeft());
        }
        if (node.getElement().compareTo(element) ==0) {
            node.getElement().incOcorrences();
        }
        if (element.compareTo(node.getElement()) > 0) {
            if (node.getRight() == null) {
                node.setRight(new Node<>(element, null, null));
                return node.getRight();
            }
            return insert(element, node.getRight());
        }
        return null;
    }

    /**
     * Returns a map with a list of words for each occurrence found.
     *
     * @return a map with a list of words for each occurrence found.
     */
    public Map<Integer, List<String>> getWordsOccurrences() {
        Map<Integer, List<String>> map = new HashMap<>();

        return map;
    }

}
