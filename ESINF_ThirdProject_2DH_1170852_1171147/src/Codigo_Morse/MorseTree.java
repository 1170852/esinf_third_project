/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Codigo_Morse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Dekas
 */
public class MorseTree extends BST<MorseType> {

    /*
    * Reads the csv file and creates the tree
     */
    public static void readMorseFile(String nomeFicheiro, BST<MorseType> morseTree) throws IOException {
        File morseCode = new File(nomeFicheiro);
        Scanner sc = new Scanner(morseCode, "UTF-8");
        morseTree.insert(new MorseType("", "Start", ""));
        while (sc.hasNext()) {
            String linha = sc.nextLine();
            String[] aux = linha.split(" ");
            morseTree.insert(new MorseType(aux[0], aux[1], aux[2]));
        }
    }

    /**
     *
     * @param input
     * @return
     */
    public String stringDecoder(String input) {
        Node<MorseType> node = root;
        String word = "";
        for (int i = 0; i < input.length(); i++) {

            if (input.charAt(i) == ' ') {
                word += node.getElement().getLetra();
                node = root;

            }
            if (input.charAt(i) == '.') {
                node = node.getLeft();
                if (i + 1 == input.length()) {
                    word += node.getElement().getLetra();
                }

            }
            if (input.charAt(i) == '_') {
                node = node.getRight();
                if (i + 1 == input.length()) {
                    word += node.getElement().getLetra();
                }
            }
        }
        return word;
    }

    public LetterTree letterTree() {
        ArrayList<MorseType> letters = new ArrayList<>();
        Iterable<MorseType> it = this.preOrder();
        for (MorseType next : it) {
            if (next.getTipo().equals("Letter")) {
                letters.add(next);
            }
        }
        LetterTree lt = new LetterTree(letters);
        return lt;
    }
    
    public String sequenciaComum(MorseType string1, MorseType string2){
        String comum = "";
        String morse1=string1.getMorse();
        String morse2=string2.getMorse();
        for (int i = 0; i< Math.min(morse1.length(),morse2.length()); i++){
            if(morse1.charAt(i)!=morse2.charAt(i)){
                break;
            }
            comum+=morse1.charAt(i);
        }
        return comum;
    }
    
    public ArrayList<String> ordenar(ArrayList<String> morses, String tipoOrdena){
        ArrayList<String> palavrasOrdenadas = new ArrayList<>();
        BST<OccurrenceTree> bst = new BST<>();
        for(String morse : morses){
            int cont=0;
            String palavra = stringDecoder(morse);
            String [] elemento = morse.split(" ");
            for(int i=0;i<elemento.length;i++){ 
                MorseType a = new MorseType(Character.toString(palavra.charAt(i)) , elemento[i]);
                Node<MorseType> m = this.find(a,this.root);
                String tipo = m.getElement().getTipo();
                if(tipo.equals(tipoOrdena)){
                    cont++;
                }
            }
            bst.insert(new OccurrenceTree(palavra, cont));
            
        }
        Iterator<OccurrenceTree> iterator = bst.inOrder().iterator();
        while(iterator.hasNext()){
            OccurrenceTree ot = iterator.next();
            if (ot.getNumOcorrencias()!=0){
                palavrasOrdenadas.add(ot.getPalavra());
            }
        }        
        return palavrasOrdenadas;
        }       
}
