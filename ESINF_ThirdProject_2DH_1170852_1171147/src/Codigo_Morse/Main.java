/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Codigo_Morse;

import PL.TREE;
import java.io.IOException;

/**
 *
 * @author Dekas
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        String ficheiro = "morse_v3.csv";
        BST<MorseType> arvore = new BST<>();
        MorseTree x = new MorseTree();
        MorseTree.readMorseFile(ficheiro,arvore);
        System.out.println(x.stringDecoder(". . ."));
        
        
    }

}
