/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Codigo_Morse;

import java.util.ArrayList;

/**
 *
 * @author Dekas
 */
public class LetterTree extends BST<MorseType> {

    public LetterTree(ArrayList<MorseType> letras) {
        for (MorseType letra : letras) {
            this.insert(letra);
        }
    }

    @Override
    public void insert(MorseType e) {
        if (isEmpty()) {
            root = new Node<>(e, null, null);
        } else {
            insert(e, root);
        }
    }

    private Node<MorseType> insert(MorseType e, Node<MorseType> node) {
        if (e.asciiCompare(node.getElement()) < 0) {
            if (node.getLeft() == null) {
                node.setLeft(new Node<>(e, null, null));
                return node.getLeft();
            }
            return insert(e, node.getLeft());
        }
        if (e.asciiCompare(node.getElement()) > 0) {
            if (node.getRight() == null) {
                node.setRight(new Node<>(e, null, null));
                return node.getRight();
            }
            return insert(e, node.getRight());
        }
        return null;
    }

    public String stringEncoder(String input) {
        String word = "";
        for (int i = 0; i < input.length(); i++) {
            Node<MorseType> noide = this.findLetter(new MorseType(Character.toString(input.charAt(i))), this.root);
            word += noide.getElement().getMorse() + " ";
        }
        return word;
    }

    public Node<MorseType> findLetter(MorseType element, Node<MorseType> node) {
        if (node == null) {
            return null;
        }
        if (element.asciiCompare(node.getElement()) == 0) {
            return node;
        }
        if (element.asciiCompare(node.getElement()) < 0) {
            return findLetter(element, node.getLeft());
        }
        return findLetter(element, node.getRight());
    }
}


