/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Codigo_Morse;

import java.util.ArrayList;

/**
 *
 * @author pedro
 */
public class OccurrenceTree implements Comparable<OccurrenceTree> {
    
    private String palavra;
    private int numOcorrencias;
    
    public OccurrenceTree(String palavra, int numOcorrencias){
        this.palavra = palavra;
        this.numOcorrencias = numOcorrencias;       
    }
    
    public String getPalavra(){
        return palavra;
    }
    
    public int getNumOcorrencias(){
        return numOcorrencias;
    }
    
    @Override
    public int compareTo(OccurrenceTree ot){
        if(this.numOcorrencias>ot.numOcorrencias){
            return -1;
        }
        else{
            return 1;
        }
    }
    
}
