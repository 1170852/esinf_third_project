/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Codigo_Morse;

/**
 *
 * @author Dekas
 */
public class MorseType implements Comparable<MorseType> {

    private String letra;
    private String morse;
    private String tipo;

    public MorseType(String morse, String letra,String tipo) {
        this.morse = morse;
        this.letra = letra;
        this.tipo = tipo;
    }   

    public MorseType(String letra) {
        this.letra = letra;
    }
    
    public MorseType(String letra, String morse) {
        this.letra = letra;
        this.morse = morse;
    }

    public void setMorse(String morse) {
        this.morse = morse;
    }

    @Override
    public int compareTo(MorseType o) {    
        int aux = Math.min(this.morse.length(), o.morse.length());
        if(this.morse.length()==0){
            if(o.morse.charAt(0) == '_'){
                return -1;
            }else{
                return 1;
            }
        }
        int i;
        for (i = 0; i < aux; i++) {
            if (this.morse.charAt(i) != o.morse.charAt(i)) {
                if (this.morse.charAt(i) == '_') {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
        if (this.morse.length() == o.morse.length()) {
            return 0;
        }
        if(this.morse.length() > o.morse.length()){
            if(this.morse.charAt(i) == '_'){
                return 1;
            } else {
                return -1;
            }
        }else{
            if(o.morse.charAt(i) == '_'){
                return -1;
            } else {
                return 1;
            }
        }
        
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.morse.equals(((MorseType) obj).morse);
    }

    @Override
    public String toString() {
        return letra;
    }

    public String getLetra() {
        return letra;
    }

    public String getTipo() {
        return tipo;
    }
    
    public int asciiCompare(MorseType o ) {
        return this.letra.compareTo(o.getLetra());
    }

    public String getMorse() {
        return morse;
    }
    
    

}
