/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Codigo_Morse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class MorseTreeTest {

    public MorseTreeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of readMorseFile method, of class MorseTree.
     */
    @Test
    public void testReadMorseFile() throws Exception {
        System.out.println("readMorseFile");
        BST<MorseType> expResult = new BST<>();
        expResult.insert(new MorseType("", "Start", ""));
        expResult.insert(new MorseType(".", "E", "Letter"));
        expResult.insert(new MorseType("_", "T", "Letter"));
        expResult.insert(new MorseType("..", "I", "Letter"));
        expResult.insert(new MorseType("._", "A", "Letter"));
        expResult.insert(new MorseType("__", "M", "Letter"));
        expResult.insert(new MorseType("_.", "N", "Letter"));
        expResult.insert(new MorseType("_..", "D", "Letter"));
        expResult.insert(new MorseType("__.", "G", "Letter"));
        expResult.insert(new MorseType("_._", "K", "Letter"));
        expResult.insert(new MorseType("___", "O", "Letter"));
        expResult.insert(new MorseType("._.", "R", "Letter"));
        expResult.insert(new MorseType("...", "S", "Letter"));
        expResult.insert(new MorseType(".._", "U", "Letter"));
        expResult.insert(new MorseType(".__", "W", "Letter"));
        Iterator<MorseType> exp = expResult.preOrder().iterator();

        String nomeFicheiro = "morse_v3_teste.csv";
        BST<MorseType> morseTree = new BST<>();
        MorseTree.readMorseFile(nomeFicheiro, morseTree);
        Iterator<MorseType> result = morseTree.preOrder().iterator();

        while (exp.hasNext()) {
            assertEquals(exp.next(), result.next());
        }
    }

    @Test
    public void testStringDecoder() throws Exception {
        String nomeFicheiro = "morse_v3_teste.csv";
        MorseTree tree = new MorseTree();
        tree.readMorseFile(nomeFicheiro, tree);
        String result = tree.stringDecoder(". .. ._.");
        String expected = "EIR";
        assertEquals(expected, result);

    }

    @Test
    public void testStringDecoderOriginal() throws Exception {
        String nomeFicheiro = "morse_v3.csv";
        MorseTree tree = new MorseTree();
        tree.readMorseFile(nomeFicheiro, tree);
        String result = tree.stringDecoder(". .. ._.");
        String expected = "EIR";
        assertEquals(expected, result);

    }

    @Test
    public void testLetterTree() throws Exception {
        String nomeFicheiro = "morse_v3_teste.csv";
        MorseTree m = new MorseTree();
        MorseTree.readMorseFile(nomeFicheiro, m);

        LetterTree result = m.letterTree();

        ArrayList<MorseType> letras = new ArrayList<>();
        MorseType m2 = new MorseType(".", "E", "Letter");
        MorseType m3 = new MorseType("._", "A", "Letter");
        MorseType m4 = new MorseType("_..", "D", "Letter");
        MorseType m5 = new MorseType("..", "I", "Letter");
        MorseType m6 = new MorseType("__.", "G", "Letter");
        MorseType m7 = new MorseType("...", "S", "Letter");
        MorseType m8 = new MorseType("._.", "R", "Letter");
        MorseType m9 = new MorseType("_.", "N", "Letter");
        MorseType m10 = new MorseType("___", "O", "Letter");
        MorseType m11 = new MorseType("_._", "K", "Letter");
        MorseType m12 = new MorseType("__", "M", "Letter");
        MorseType m13 = new MorseType(".._", "U", "Letter");
        MorseType m14 = new MorseType("_", "T", "Letter");
        MorseType m15 = new MorseType(".__", "W", "Letter");
        letras.add(m2);
        letras.add(m3);
        letras.add(m4);
        letras.add(m5);
        letras.add(m6);
        letras.add(m7);
        letras.add(m8);
        letras.add(m9);
        letras.add(m10);
        letras.add(m11);
        letras.add(m12);
        letras.add(m13);
        letras.add(m14);
        letras.add(m15);

        LetterTree expected = new LetterTree(letras);
        Iterator<MorseType> it = result.preOrder().iterator();
        Iterator<MorseType> it2 = expected.preOrder().iterator();
        while (it.hasNext()) {
            assertEquals(it.next(), it2.next());

        }
    }

    @Test
    public void testSequenciaComum() throws Exception {
        String nomeFicheiro = "morse_v3.csv";
        MorseTree tree = new MorseTree();
        tree.readMorseFile(nomeFicheiro, tree);

        //Length String1 = String2
        String result = tree.sequenciaComum(new MorseType(".___", "J", "Letter"), new MorseType(".__.", "P", "Letter"));
        String expected = ".__";
        assertEquals(expected, result);

        //Nada em comum
        result = tree.sequenciaComum(new MorseType(".", "E", "Letter"), new MorseType("_", "T", "Letter"));
        expected = "";
        assertEquals(expected, result);

        //Length String1 maior que String2
        result = tree.sequenciaComum(new MorseType(".....", "5", "Number"), new MorseType(".___", "J", "Letter"));
        expected = ".";
        assertEquals(expected, result);

        //Length String1 menor que String2
        result = tree.sequenciaComum(new MorseType("....", "H", "Letter"), new MorseType(".___", "J", "Letter"));
        expected = ".";
        assertEquals(expected, result);
    }

    @Test
    public void testStringEncoder() throws IOException {
        String nomeFicheiro = "morse_v3.csv";
        MorseTree m = new MorseTree();
        MorseTree.readMorseFile(nomeFicheiro, m);
        LetterTree tree = m.letterTree();
        String toEncode = "ESINF";
        String expected = ". ... .. _. .._. ";
        String result = tree.stringEncoder(toEncode);
        assertEquals(expected, result);
        
    }
    
    @Test
    public void testOrdenar() throws Exception { 
        String nomeFicheiro = "morse_v3.csv";
        MorseTree tree = new MorseTree();
        tree.readMorseFile(nomeFicheiro, tree);
        ArrayList<String>morseList=new ArrayList<>();
        morseList.add(".____ ..___ ...__");
        morseList.add("._ ..___ ...__");
        morseList.add("._ _... ...__");
        morseList.add("._ _... _._.");
        ArrayList<String> result = tree.ordenar(morseList, "Letter");
        ArrayList<String> expected = new ArrayList<>();
        expected.add("ABC");
        expected.add("AB3");
        expected.add("A23");
        assertEquals(expected, result);
        
        result = tree.ordenar(morseList, "Number");
        expected = new ArrayList<>();
        expected.add("123");
        expected.add("A23");
        expected.add("AB3");
        assertEquals(expected, result);
        
        result = tree.ordenar(morseList, "Non-English");
        expected = new ArrayList<>();
        assertEquals(expected, result);
        
    }
}
